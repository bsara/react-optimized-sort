/*!
 * react-optimized-sort 2.0.1
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-optimized-sort/blob/master/LICENSE)
 */
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var PropTypes = _interopDefault(require('prop-types'));
var getValue = _interopDefault(require('get-value'));
var objectHash = require('object-hash');

class OptimizedSort extends React.Component {

  constructor(...args) {
    super(...args);

    this._lastItems           = undefined;
    this._lastComparator      = undefined;
    this._lastComparators     = undefined;
    this._lastComparatorsHash = undefined;

    this._cachedSortedItems                   = undefined;
    this._cachedGeneratedComparatorConfigHash = undefined;
    this._cachedGeneratedComparator           = undefined;
  }



  render() {
    const renderFunc = (
      (this.props.children != null)
        ? this.props.children
        : this.props.render
    );

    return renderFunc(this._sortItems());
  }





  _sortItems() {
    if (this.props.skip) {
      return this.props.items;
    }

    const comparatorsHash = ((this.props.comparators == null) ? undefined : objectHash.MD5(this.props.comparators));

    if (this.props.ignoreCache
        || this.props.items !== this._lastItems
        || this.props.comparator !== this._lastComparator
        || (this.props.comparators !== this._lastComparators && (this._lastComparatorsHash == null || comparatorsHash !== this._lastComparatorsHash))) {
      this._setCachedSortedItems();

      this._lastItems           = this.props.items;
      this._lastComparator      = this.props.comparator;
      this._lastComparators     = this.props.comparators;
      this._lastComparatorsHash = comparatorsHash;
    }

    return this._cachedSortedItems;
  }



  _setCachedSortedItems() {
    if (this.props.items == null || !this.props.items.length || (this.props.comparator == null && this.props.comparators == null)) {
      this._cachedSortedItems = this.props.items;
      return;
    }

    const finalItems      = [...this.props.items];
    const finalSorter     = (this.props.sorter || _defaultSorter);
    const finalComparator = (this.props.comparator || this._getComparatorConfigsComparator());

    this._cachedSortedItems = finalSorter(finalItems, (...sortComparatorArgs) => finalComparator(...sortComparatorArgs, {
      defaultIsAscending: this.props.ascending,
      defaultNullsFirst:  this.props.nullsfirst
    }));


    if (!Array.isArray(this._cachedSortedItems)) {
      this._cachedSortedItems = finalItems;
    }
  }



  _getComparatorConfigsComparator() {
    const comparatorsHash = objectHash.MD5(this.props.comparators);

    if (comparatorsHash !== this._cachedGeneratedComparatorConfigHash) {
      this._cachedGeneratedComparator           = _createComparatorFromComparatorConfigs(this.props.comparators);
      this._cachedGeneratedComparatorConfigHash = comparatorsHash;
    }

    return this._cachedGeneratedComparator;
  }


}


OptimizedSort.propTypes = {
  items: PropTypes.array,

  render:   PropTypes.func,
  children: PropTypes.func,

  sorter:      PropTypes.func,
  comparator:  PropTypes.func,
  comparators: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.oneOfType([
      PropTypes.shape({
        comparator: PropTypes.func.isRequired,
        options:    PropTypes.any
      }),

      PropTypes.shape({
        propPath: PropTypes.string.isRequired,
        type:     PropTypes.oneOf(['string', 'number', 'date', 'boolean']).isRequired,
        options:  PropTypes.shape({
          isAscending: PropTypes.bool,
          nullsFirst:  PropTypes.bool,



          localeIds:         PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
          usage:             PropTypes.string,
          localeMatcher:     PropTypes.string,
          numeric:           PropTypes.bool,
          caseFirst:         PropTypes.string,
          sensitivity:       PropTypes.string,
          ignorePunctuation: PropTypes.bool

        })
      })
    ])
  ])),

  ascending:  PropTypes.bool,
  nullsFirst: PropTypes.bool,

  skip:        PropTypes.bool,
  ignoreCache: PropTypes.bool
};


OptimizedSort.defaultProps = {
  sorter: _defaultSorter
};






function _defaultSorter(array, comparator) {
  return array.sort(comparator);
}



function _createComparatorFromComparatorConfigs(comparatorConfigs) {
  const propComparators = comparatorConfigs.map(_createComparatorFromComparatorConfig);

  return function(...args) {
    for (let i = 0; i < propComparators.length; i++) {
      const comparisonResult = propComparators[i](...args);

      if (comparisonResult !== 0) {
        return comparisonResult;
      }
    }

    return undefined;
  };
}



function _createComparatorFromComparatorConfig(comparatorConfig) {
  let comparator = comparatorConfig;

  if (typeof comparatorConfig === 'object') {
    comparator = (
      (comparatorConfig.comparator != null)
        ? _createFunctionComparatorWithOptions(comparatorConfig)
        : _createTypePropComparator(comparatorConfig)
    );
  }

  return function(leftItem, rightItem, comparatorOptions) {
    const comparatorConfigOptions = (comparatorConfig.options || {});

    const finalIsAscending = ((comparatorConfigOptions.isAscending == null) ? comparatorOptions.defaultIsAscending : comparatorConfigOptions.isAscending);
    const result           = comparator(leftItem, rightItem, Object.assign({}, comparatorConfigOptions, comparatorOptions));

    return ((finalIsAscending == null || finalIsAscending) ? result : (result * -1));
  };
}



function _createFunctionComparatorWithOptions({ comparator, options: comparatorConfigOptions }) {
  return function(leftItem, rightItem, comparatorOptions) {
    return comparator(leftItem, rightItem, Object.assign({}, comparatorConfigOptions, comparatorOptions));
  };
}



function _createTypePropComparator({ propPath, type, options: { isAscending, nullsFirst, ...comparatorConfigOptions } = {} }) {
  let comparator = null;

  switch (type) {
    case 'string':
      comparator = _stringComparator;
      break;

    case 'number':
      comparator = _numberComparator;
      break;

    case 'date':
      comparator = _dateComparator;
      break;

    case 'boolean':
      comparator = _booleanComparator;
      break;

    default:
      throw new Error(`Comparator 'type' given was invalid. Possible values are 'string', 'number', 'date', and 'boolean'.`);
  }


  return function(leftItem, rightItem, comparatorOptions) {
    const { defaultNullsFirst } = comparatorOptions;


    if (leftItem === rightItem) {
      return 0;
    }


    const leftItemPropValue  = getValue(leftItem, propPath);
    const rightItemPropValue = getValue(rightItem, propPath);

    if (leftItemPropValue === rightItemPropValue) {
      return 0;
    }


    const finalNullsFirst = ((nullsFirst == null) ? defaultNullsFirst : nullsFirst);

    if (leftItem == null || leftItemPropValue == null) {
      return (finalNullsFirst ? -1 : 1);
    }
    if (rightItem == null || rightItemPropValue == null) {
      return (finalNullsFirst ? 1 : -1);
    }


    return comparator(leftItemPropValue, rightItemPropValue, Object.assign({}, comparatorConfigOptions, comparatorOptions));
  };
}



function _stringComparator(leftItemPropValue, rightItemPropValue, { localeIds, ...localeCompareOptions }) {
  return leftItemPropValue.localeCompare(rightItemPropValue, localeIds, localeCompareOptions);
}



function _numberComparator(leftItemPropValue, rightItemPropValue) {
  return (Number(leftItemPropValue) - Number(rightItemPropValue));
}



function _dateComparator(leftItemPropValue, rightItemPropValue) {
  return (new Date(leftItemPropValue) - new Date(rightItemPropValue));
}



function _booleanComparator(leftItemPropValue, rightItemPropValue) {
  return (!leftItemPropValue - !rightItemPropValue);
}

module.exports = OptimizedSort;
