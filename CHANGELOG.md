# 2.0.0

- **[BREAKING CHANGE]** Class syntax is now used without being transpiled.
- Updated some file names and converted to using `rollup` for build.
- Upgraded to latest versions of dev dependencies.



# 1.0.2

- README update.



# 1.0.1

- Initial release. (forked from `@bsara/react-sort`)
