/* eslint-disable react/jsx-no-bind */
/** !
 * ISC License (ISC)
 *
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

import React from 'react';
import { storiesOf } from '@storybook/react';

import OptimizedSort from '.';



const initialItems = [
  { id: 15, name: "apricot" },
  { id: 20, name: "kiwi" },
  { id: 30, name: "raspberry" },
  { id: 7, name: "plum" },
  { id: 32, name: "lemon" },
  { id: 5, name: "orange" },
  { id: 22, name: "peach" },
  { id: 17, name: "pomegranate" },
  { id: 28, name: "tangerine" },
  { id: 21, name: "blackberry" },
  { id: 13, name: "nectarine" },
  { id: 19, name: "papaya" },
  { id: 29, name: "huckleberry" },
  { id: 4, name: "cherry" },
  { id: 26, name: "mango" },
  { id: 14, name: "gooseberry" },
  { id: 2, name: "strawberry" },
  { id: 0, name: "watermelon" },
  { id: 25, name: "fig" },
  { id: 8, name: "guava" },
  { id: 27, name: "lime" },
  { id: 12, name: "clementine" },
  { id: 11, name: "boysenberry" },
  { id: 1, name: "grape" },
  { id: 6, name: "banana" },
  { id: 3, name: "honeydew" },
  { id: 16, name: "blueberry" },
  { id: 23, name: "grapefruit" },
  { id: 31, name: "cantaloupe" },
  { id: 9, name: "apple" },
  { id: 18, name: "pear" },
  { id: 10, name: "passion fruit" },
  { id: 24, name: "pineapple" }
];


const _sortByIdComparators = [
  {
    propPath: 'id',
    type:     'number'
  }
];

const _sortByNameComparators = [
  {
    propPath: 'name',
    type:     'string'
  }
];




class _TestComponent extends React.Component {

  constructor(...args) {
    super(...args);

    this.state = {
      skip:        false,
      ignoreCache: false,
      comparators: undefined
    };
  }



  render() {
    return (
      <div>
        <div>
          <a href="https://gitlab.com/bsara/react-optimized-sort/blob/master/story.jsx#L120-129">View story source</a>
        </div>
        <br />
        <div>
          <div>
            <input id="skipCheckbox" type="checkbox" onChange={() => this.setState({ skip: !this.state.skip })} />
            <label htmlFor="skipCheckbox">&nbsp;Skip Sorting</label>
          </div>
          <br />
          <div>
            <input id="ignoreCache" type="checkbox" onChange={() => this.setState({ ignoreCache: !this.state.ignoreCache })} />
            <label htmlFor="ignoreCache">&nbsp;Ignore Cache</label>
          </div>
          <br />
          <button disabled={this.state.comparators == null} onClick={() => this.setState({ comparators: null })}>Unsort</button>
          &nbsp;
          <button disabled={this.state.comparators === _sortByNameComparators} onClick={() => this.setState({ comparators: _sortByNameComparators })}>Sort by Name</button>
          &nbsp;
          <button disabled={this.state.comparators === _sortByIdComparators} onClick={() => this.setState({ comparators: _sortByIdComparators })}>Sort by Id</button>
        </div>
        <hr />
        <div>
          <OptimizedSort
            items={initialItems}
            skip={this.state.skip}
            ignoreCache={this.state.ignoreCache}
            comparators={this.state.comparators}
            render={(sortedItems) => (
              <React.Fragment key="filtered-items-fragment">
                {sortedItems.map((item) => <div key={item.id}><pre>{item.name.padEnd(13, " ")} [id = {item.id.toString().padStart(2, "0")}]</pre></div>)}
              </React.Fragment>
            )} />
        </div>
      </div>
    );
  }
}



storiesOf("react-optimized-sort", module)
  .add("Default", () => <_TestComponent />);
